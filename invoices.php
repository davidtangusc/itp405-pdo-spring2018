<?php
  $pdo = new PDO('sqlite:chinook.db');
  $statement = $pdo->prepare('
    select *
    from invoices
    inner join customers
    on invoices.CustomerId = customers.CustomerId
    order by InvoiceDate desc
  ');
  $statement->execute();
  $invoices = $statement->fetchAll(PDO::FETCH_OBJ);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Invoices</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css">
  </head>
  <body>
    <table class="table">
      <tr>
        <th>Date</th>
        <th>Total</th>
        <th>Name</th>
        <th colspan="2">Email</th>
      </tr>
      <?php foreach($invoices as $invoice) : ?>
        <tr>
          <td><?php echo $invoice->InvoiceDate ?></td>
          <td>$<?php echo $invoice->Total ?></td>
          <td><?php echo $invoice->FirstName . ' ' . $invoice->LastName ?></td>
          <td><?php echo $invoice->Email ?></td>
          <td>
            <a href="invoice-details.php?invoice=<?php echo $invoice->InvoiceId ?>">Details</a>
          </td>
        </tr>
      <?php endforeach ?>
    </table>
  </body>
</html>
